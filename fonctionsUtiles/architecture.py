## Code taken from the DonkeyCar project
## Thanks to W. Roscoe and the DonkeyCar community

def model_categorical_leaky_relu(input_size= (90,250,3)  , dropout=0.25, alpha = 1):
    '''Modèle catégorial avec une activation Leaky_Relu
    '''
    import keras
    from keras.layers import Input, Dense, merge
    from keras.models import Model
    from keras.layers import Conv2D, MaxPooling2D, Reshape, BatchNormalization
    from keras.layers import Activation, Dropout, Flatten, Dense , LSTM , concatenate , core, Reshape , LeakyReLU


    
    img_in = Input(shape=input_size, name='img_in')                      # First layer, input layer, Shape comes from camera.py resolution, RGB
    x = img_in
    
    x = Conv2D(24, 5,5, subsample=(2,2))(x)       # 24 features, 5 pixel x 5 pixel kernel (convolution, feauture) window, 2wx2h stride, relu activation
    x = BatchNormalization()(x)
    x = LeakyReLU(alpha = alpha)(x)
    
    x = Conv2D(32, 5,5, subsample=(2,2))(x)   # 32 features, 5px5p kernel window, 2wx2h stride, relu activatiion
    x = BatchNormalization()(x)
    x = LeakyReLU(alpha = alpha)(x)
    
    x = Conv2D(64, 5,5, subsample=(2,2))(x)     # 64 features, 5px5p kernal window, 2wx2h stride, relu
    x = BatchNormalization()(x)
    x = LeakyReLU(alpha = alpha)(x)
    
    x = Conv2D(64, (3,3) , subsample=(2,2))(x)       # 64 features, 3px3p kernal window, 2wx2h stride, relu
    x = BatchNormalization()(x)
    x = LeakyReLU(alpha = alpha)(x)
    
    x = Conv2D(64, (3,3) , subsample=(1,1))(x)    # 64 features, 3px3p kernal window, 1wx1h stride, relu
    x = BatchNormalization()(x)
    x =LeakyReLU(alpha = alpha)(x)
       
    x = Dropout(dropout)(x)

    # Possibly add MaxPooling (will make it less sensitive to position in image).  Camera angle fixed, so may not to be needed

    x = Flatten(name='flattened')(x)                                        # Flatten to 1D (Fully connected)
    x = Dense(1024, activation='relu')(x)                                    # Classify the data into 100 features, make all negatives 0
    x = Dropout(dropout)(x)                                                      # Randomly drop out (turn off) 10% of the neurons (Prevent overfitting)
    x = Dense(50, activation='relu')(x)                                     # Classify the data into 50 features, make all negatives 0
    x = Dropout(dropout)(x)                                                      # Randomly drop out 10% of the neurons (Prevent overfitting)
    
    

    
    #categorical output of the angle
    angle_out = Dense(5, activation='softmax', name='angle_out')(x)      
    
    model = Model(inputs = img_in, outputs = angle_out)
    
    model.compile(optimizer='adam',
                  loss={
                      'angle_out' : 'categorical_crossentropy'                       } ,
                  
                  metrics={
                      'angle_out' : 'accuracy'                          }
                           )

    return model







def model_categorical(input_size= (90,250,3)  , dropout=0.1):
    '''Generate an NVIDIA AutoPilot architecture.
    Input_size: Image shape (90, 250, 3), adjust to your desired input.
    Dropout: Proportion of dropout used to avoid model overfitting.
    This model ONLY predicts steering angle as a 5-elements array encoded with a Softmax output.
    The model is already compiled and ready to be trained.
    '''
    import keras
    from keras.layers import Input, Dense, merge
    from keras.models import Model
    from keras.layers import Conv2D, MaxPooling2D, Reshape, BatchNormalization
    from keras.layers import Activation, Dropout, Flatten, Dense , LSTM , concatenate , core, Reshape


    
    img_in = Input(shape=input_size, name='img_in')                      # First layer, input layer, Shape comes from camera.py resolution, RGB
    x = img_in
    
    x = Conv2D(24, 5,5, subsample=(2,2))(x)       # 24 features, 5 pixel x 5 pixel kernel (convolution, feauture) window, 2wx2h stride, relu activation
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(32, 5,5, subsample=(2,2))(x)   # 32 features, 5px5p kernel window, 2wx2h stride, relu activatiion
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, 5,5, subsample=(2,2))(x)     # 64 features, 5px5p kernal window, 2wx2h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, (3,3) , subsample=(2,2))(x)       # 64 features, 3px3p kernal window, 2wx2h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, (3,3) , subsample=(1,1))(x)    # 64 features, 3px3p kernal window, 1wx1h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
       
    x = Dropout(dropout)(x)

    # Possibly add MaxPooling (will make it less sensitive to position in image).  Camera angle fixed, so may not to be needed

    x = Flatten(name='flattened')(x)                                        # Flatten to 1D (Fully connected)
    x = Dense(1024, activation='relu')(x)                                   
    x = Dropout(dropout)(x)                                                      # Randomly drop out (turn off) 10% of the neurons (Prevent overfitting)
    x = Dense(50, activation='relu')(x)                                     # Classify the data into 50 features, make all negatives 0
    x = Dropout(dropout)(x)                                                      # Randomly drop out 10% of the neurons (Prevent overfitting)
    
    

    
    #categorical output of the angle
    angle_out = Dense(5, activation='softmax', name='angle_out')(x)       
    model = Model(inputs = img_in, outputs = angle_out)
    
    model.compile(optimizer='adam',
                  loss={
                      'angle_out' : 'categorical_crossentropy'                       } ,
                  
                  metrics={
                      'angle_out' : 'accuracy'                          }
                           )

    return model



def model_categorical_one_inout (input_size= (96,160,3) , dropout=0.5):
    '''Generate an NVIDIA AutoPilot architecture.
    Input_size: Image shape (90, 250, 3), adjust to your desired input.
    Dropout: Proportion of dropout used to avoid model overfitting.
    This model ONLY predicts steering angle as a 5-elements array encoded with a Softmax output.
    The model is already compiled and ready to be trained.
    '''
    import keras
    from keras.layers import Input, Dense, merge
    from keras.models import Model
    from keras.layers import Conv2D, MaxPooling2D, Reshape, BatchNormalization
    from keras.layers import Activation, Dropout, Flatten, Dense , LSTM , concatenate , core, Reshape
    from keras.backend import shape


    
    img_in = Input(shape=input_size, name='img_in')                      # First layer, input layer, Shape comes from camera.py resolution, RGB
    #speed_in = Input(shape = input_size_speed , name = 'speed_in')

    x = img_in
    #y = speed_in

    x = Conv2D(24, 5,5, subsample=(2,2))(x)       # 24 features, 5 pixel x 5 pixel kernel (convolution, feauture) window, 2wx2h stride, relu activation
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(32, 5,5, subsample=(2,2))(x)   # 32 features, 5px5p kernel window, 2wx2h stride, relu activatiion
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, 5,5, subsample=(2,2))(x)     # 64 features, 5px5p kernal window, 2wx2h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, (3,3) , subsample=(2,2))(x)       # 64 features, 3px3p kernal window, 2wx2h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, (3,3) , subsample=(1,1))(x)    # 64 features, 3px3p kernal window, 1wx1h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
       
    x = Dropout(dropout)(x) # Possibly add MaxPooling (will make it less sensitive to position in image).  Camera angle fixed, so may not to be needed

    x = Flatten(name='flattened')(x)# Flatten to 1D (Fully connected)
    x = Dense(1024, activation='relu')(x)
    x = Dropout(dropout)(x)
    # Classify the data into 100 features, make all negatives 0
                                                         # Randomly drop out (turn off) 10% of the neurons (Prevent overfitting)
    #print('la taille de x output de la couche dense qui suit la convolution est:' , shape(x))
    
    """x = BatchNormalization()(x)
    x = Reshape((1 , 1024))(x)
    x = LSTM(50 , activation = 'tanh')(x)
    x = Dropout(dropout)(x) 
    """
    x = Dense(100 , use_bias = False)(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = Dropout(dropout)(x)
    
    x = Dense(50 , use_bias = False)(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = Dropout(dropout)(x)
    
    speed_out = Dense(1 , activation = 'sigmoid' , name = 'speed_out')(x)
    
    model = Model(inputs = img_in, outputs = speed_out)
    
    model.compile(optimizer = 'adam',
                  loss = {
                      'speed_out' : 'binary_crossentropy'
                       } ,
                  
                  metrics = {
                      'speed_out' : 'accuracy'
                          }
                           )

    return model

def model_categorical_one_inout_9conv (input_size= (96,160,3) , dropout=0.5):
    '''Generate an NVIDIA AutoPilot architecture.
    Input_size: Image shape (90, 250, 3), adjust to your desired input.
    Dropout: Proportion of dropout used to avoid model overfitting.
    This model ONLY predicts steering angle as a 5-elements array encoded with a Softmax output.
    The model is already compiled and ready to be trained.
    '''
    import keras
    from keras.layers import Input, Dense, merge
    from keras.models import Model
    from keras.layers import Conv2D, MaxPooling2D, Reshape, BatchNormalization
    from keras.layers import Activation, Dropout, Flatten, Dense , LSTM , concatenate , core, Reshape
    from keras.backend import shape


    
    img_in = Input(shape=input_size, name='img_in')                      # First layer, input layer, Shape comes from camera.py resolution, RGB
    #speed_in = Input(shape = input_size_speed , name = 'speed_in')

    x = img_in
    #y = speed_in

    x = Conv2D(24, 5,5, subsample=(2,2))(x)       # 24 features, 5 pixel x 5 pixel kernel (convolution, feauture) window, 2wx2h stride, relu activation
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(32, 5,5, subsample=(2,2))(x)   # 32 features, 5px5p kernel window, 2wx2h stride, relu activatiion
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, 5,5, subsample=(2,2))(x)     # 64 features, 5px5p kernal window, 2wx2h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, (3,3) , padding = 'same')(x)       # 64 features, 3px3p kernal window, 2wx2h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, (3,3) , padding = 'same')(x)    # 64 features, 3px3p kernal window, 1wx1h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, (3,3 ), padding = 'same')(x)    # 64 features, 3px3p kernal window, 1wx1h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, (3,3) , padding = 'same')(x)    # 64 features, 3px3p kernal window, 1wx1h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, (3,3) , padding = 'same')(x)    # 64 features, 3px3p kernal window, 1wx1h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, (3,3) , padding = 'same')(x)    # 64 features, 3px3p kernal window, 1wx1h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    
    x = Dropout(dropout)(x) 
    # Possibly add MaxPooling (will make it less sensitive to position in image).  Camera angle fixed, so may not to be needed

    x = Flatten(name='flattened')(x)                                        # Flatten to 1D (Fully connected)
    x = Dense(1024, activation='relu')(x)                                    # Classify the data into 100 features, make all negatives 0
                                                         # Randomly drop out (turn off) 10% of the neurons (Prevent overfitting)
    #print('la taille de x output de la couche dense qui suit la convolution est:' , shape(x))
    
    x = BatchNormalization()(x)
    x = Reshape((1 , 1024))(x)
    x = LSTM(50 , activation = 'tanh')(x)
    x = Dropout(dropout)(x) 
    
    x = Dense(100 , use_bias = False)(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = Dropout(dropout)(x)
    
    x = Dense(50 , use_bias = False)(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = Dropout(dropout)(x)
    
    speed_out = Dense(2 , activation = 'softmax' , name = 'speed_out')(x)
    
    model = Model(inputs = img_in, outputs = speed_out)
    
    model.compile(optimizer = 'adam',
                  loss = {
                      'speed_out' : 'categorical_crossentropy'
                       } ,
                  
                  metrics = {
                      'speed_out' : 'accuracy'
                          }
                           )

    return model



def model_categorial_twoin_twoout(input_size= (90,250,3), input_size_speed = (None, 1) , dropout=0.1):

    from keras.layers import Input, Dense, merge
    from keras.models import Model
    from keras.layers import Conv2D, MaxPooling2D, Reshape, BatchNormalization
    from keras.layers import Activation, Dropout, Flatten, Dense, LSTM , concatenate


    
    img_in = Input(shape=input_size, name='img_in')
    speed_in = Input(shape = input_size_speed , name = 'speed_in')
    
    # First layer, input layer, Shape comes from camera.py resolution, RGB
    x = img_in
    
    x = Conv2D(24, 5,5, subsample=(2,2))(x)       # 24 features, 5 pixel x 5 pixel kernel (convolution, feauture) window, 2wx2h stride, relu activation
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(32, 5,5, subsample=(2,2))(x)   # 32 features, 5px5p kernel window, 2wx2h stride, relu activatiion
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, 5,5, subsample=(2,2))(x)     # 64 features, 5px5p kernal window, 2wx2h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, 3,3, subsample=(1,1))(x)       # 64 features, 3px3p kernal window, 2wx2h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, 3,3, subsample=(1,1))(x)    # 64 features, 3px3p kernal window, 1wx1h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)

    # Possibly add MaxPooling (will make it less sensitive to position in image).  Camera angle fixed, so may not to be needed

    x = Flatten(name='flattened')(x)                                        # Flatten to 1D (Fully connected)
    x = Dense(100, activation='relu')(x)                                    # Classify the data into 100 features, make all negatives 0
    x = Dropout(dropout)(x)                                                      # Randomly drop out (turn off) 10% of the neurons (Prevent overfitting)
    x = Dense(50, activation='relu')(x)                                     # Classify the data into 50 features, make all negatives 0
    x = Dropout(dropout)(x)                                                      # Randomly drop out 10% of the neurons (Prevent overfitting)
    
    y = speed_in
    y = BatchNormalization(axis = -1)(y)
    y = LSTM(50 , activation = 'relu')(y)
    y = Dense(50 , activation = 'relu')(y)
    y = Dense(50 , activation = 'relu')(y)
    
    y = concatenate([x , y] , axis = 1)
    y = Dense(50 , activation = 'relu')(y)
    
    
    #categorical output of the angle
    angle_out = Dense(1, activation='linear', name='angle_out')(x)        # Connect every input with every output and output 15 hidden units. Use Softmax to give percentage. 15 categories and find best one based off percentage 0.0-1.0
    speed_out = Dense(2 , activation = 'softmax' , name = 'speed_out')(y)
    
    model = Model(input=[img_in , speed_in], output=[angle_out , speed_out])
    
    model.compile(optimizer='adam',
                  
                  loss={'angle_out':'mean_squared_error' , 
                        'speed_out' : 'categorical_crossentropy'},
                  
                  metrics={'angle_out' : 'mean_absolute_error',
                           'speed_out' : 'accuracy'}
                 )


    return model





def model_regression(input_size= (90,250,3)  , dropout=0.1):
    '''Generate an NVIDIA AutoPilot architecture.
    Input_size: Image shape (90, 250, 3), adjust to your desired input.
    Dropout: Proportion of dropout used to avoid model overfitting.
    This model ONLY predicts steering angle as a 5-elements array encoded with a Softmax output.
    The model is already compiled and ready to be trained.
    '''
    import keras
    from keras.layers import Input, Dense, merge
    from keras.models import Model
    from keras.layers import Conv2D, MaxPooling2D, Reshape, BatchNormalization
    from keras.layers import Activation, Dropout, Flatten, Dense , LSTM , concatenate , core, Reshape


    
    img_in = Input(shape=input_size, name='img_in')                      # First layer, input layer, Shape comes from camera.py resolution, RGB
    x = img_in
    
    x = Conv2D(24, 5,5, subsample=(2,2))(x)       # 24 features, 5 pixel x 5 pixel kernel (convolution, feauture) window, 2wx2h stride, relu activation
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(32, 5,5, subsample=(2,2))(x)   # 32 features, 5px5p kernel window, 2wx2h stride, relu activatiion
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, 5,5, subsample=(2,2))(x)     # 64 features, 5px5p kernal window, 2wx2h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, (3,3) , subsample=(2,2))(x)       # 64 features, 3px3p kernal window, 2wx2h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, (3,3) , subsample=(1,1))(x)    # 64 features, 3px3p kernal window, 1wx1h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
       
    x = Dropout(dropout)(x)

    # Possibly add MaxPooling (will make it less sensitive to position in image).  Camera angle fixed, so may not to be needed

    x = Flatten(name='flattened')(x)                                        # Flatten to 1D (Fully connected)
    x = Dense(1024, activation='relu')(x)                                    # Classify the data into 100 features, make all negatives 0
    x = Dropout(dropout)(x)                                                      # Randomly drop out (turn off) 10% of the neurons (Prevent overfitting)
    x = Dense(50, activation='relu')(x)                                     # Classify the data into 50 features, make all negatives 0
    x = Dropout(dropout)(x)                                                      # Randomly drop out 10% of the neurons (Prevent overfitting)
    
    

    
    #categorical output of the angle
    angle_out = Dense(1, activation= 'linear', name='angle_out')(x)        # Connect every input with every output and output 15 hidden units. Use Softmax to give percentage. 15 categories and find best one based off percentage 0.0-1.0
    
    model = Model(inputs = img_in, outputs = angle_out)
    
    model.compile(optimizer='adam',
                  loss={
                      'angle_out':'mean_squared_error'                        } ,
                  
                  metrics={
                      'angle_out' : 'mean_absolute_error'                          }
                           )

    return model



def model_categorical_onein_twoout (input_size= (90,250,3) , dropout=0.1):
    '''Generate an NVIDIA AutoPilot architecture.
    Input_size: Image shape (90, 250, 3), adjust to your desired input.
    Dropout: Proportion of dropout used to avoid model overfitting.
    This model ONLY predicts steering angle as a 5-elements array encoded with a Softmax output.
    The model is already compiled and ready to be trained.
    '''
    #import tensorflow as tf

    #config = tf.ConfigProto()
    #config.gpu_options.allow_growth = True
    #tf.keras.backend.set_session(tf.Session(config=config))
    
    #import keras
    from keras.layers import Input, Dense, merge
    from keras.models import Model
    from keras.layers import Conv2D, MaxPooling2D, Reshape, BatchNormalization
    from keras.layers import Activation, Dropout, Flatten, Dense, LSTM , concatenate
    
    img_in = Input(shape=input_size, name='img_in')
    
    x = Conv2D(24, 5,5, subsample=(2,2))(img_in)       # 24 features, 5 pixel x 5 pixel kernel (convolution, feauture) window, 2wx2h stride, relu activation
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(32, 5,5, subsample=(2,2))(x)   # 32 features, 5px5p kernel window, 2wx2h stride, relu activatiion
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, 5,5, subsample=(2,2))(x)     # 64 features, 5px5p kernal window, 2wx2h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, (3,3) , subsample=(2,2))(x)       # 64 features, 3px3p kernal window, 2wx2h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Conv2D(64, (3,3) , subsample=(1,1))(x)    # 64 features, 3px3p kernal window, 1wx1h stride, relu
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    
    x = Dropout(dropout)(x) 
    
    x = Flatten(name='flattened')(x)                                        # Flatten to 1D (Fully connected)
    x = Dense(1024, activation='relu')(x) 
    x = BatchNormalization()(x)
    
    #x = Reshape((1 , 1024))(x)
#_________________________________________________________________________________________________________________    
    #y = LSTM(50 , activation = 'tanh')(x)
    #y = Dropout(dropout)(y) 
    
    y = Dense(100 , use_bias = False)(x)
    y = BatchNormalization()(y)
    y = Activation('relu')(y)
    y = Dropout(dropout)(y)
    
    y = Dense(50 , use_bias = False)(y)
    y = BatchNormalization()(y)
    y = Activation('relu')(y)
    y = Dropout(dropout)(y)
    
    speed_out = Dense(1 , activation = 'sigmoid' , name = 'speed_out')(y)

#___________________________________________________________________________________________________________________
    #z = LSTM(50 , activation = 'tanh')(x)
    #z = Dropout(dropout)(z)
    
    z = Dense(100 , use_bias = False)(x)
    z = BatchNormalization()(z)
    z = Activation('relu')(z)
    z = Dropout(dropout)(z)
    
    z = Dense(50 , use_bias = False)(z)
    z = BatchNormalization()(z)
    z = Activation('relu')(z)
    z = Dropout(dropout)(z)
    
    
    angle_out = Dense(5, activation='softmax', name='angle_out')(z)    
#__________________________________________________________________________________________________________________

    model = Model(input=img_in , output=[angle_out , speed_out])
    
    model.compile(optimizer='adam',
                  
                  loss={'angle_out':'categorical_crossentropy' , 
                        'speed_out' : 'binary_crossentropy'},
                  
                  metrics={'angle_out' : 'accuracy',
                           'speed_out' : 'accuracy'}
                 )

    return model



def model_categorical_canny(input_size= (90,250), dropout=0.1):
    '''Generate an NVIDIA AutoPilot architecture.
    Input_size: Image shape (90, 250, 3), adjust to your desired input.
    Dropout: Proportion of dropout used to avoid model overfitting.
    This model ONLY predicts steering angle as a 5-elements array encoded with a Softmax output.
    The model is already compiled and ready to be trained.
    '''
    import keras
    from keras.layers import Input, Dense, merge
    from keras.models import Model
    from keras.layers import Conv1D, Conv2D, MaxPooling2D, Reshape, BatchNormalization
    from keras.layers import Activation, Dropout, Flatten, Dense


    
    img_in = Input(shape=input_size, name='img_in')                      # First layer, input layer, Shape comes from camera.py resolution, RGB
    x = img_in
    x = Conv1D(24, 5,5, subsample=(2,2), activation='relu')(x)       # 24 features, 5 pixel x 5 pixel kernel (convolution, feauture) window, 2wx2h stride, relu activation
    x = Conv1D(32, 5,5, subsample=(2,2), activation='relu')(x)       # 32 features, 5px5p kernel window, 2wx2h stride, relu activatiion
    x = Conv1D(64, 5,5, subsample=(2,2), activation='relu')(x)       # 64 features, 5px5p kernal window, 2wx2h stride, relu
    x = Conv1D(64, 3,3, subsample=(1,1), activation='relu')(x)       # 64 features, 3px3p kernal window, 2wx2h stride, relu
    x = Conv1D(64, 3,3, subsample=(1,1), activation='relu')(x)       # 64 features, 3px3p kernal window, 1wx1h stride, relu

    # Possibly add MaxPooling (will make it less sensitive to position in image).  Camera angle fixed, so may not to be needed

    x = Flatten(name='flattened')(x)                                        # Flatten to 1D (Fully connected)
    x = Dense(100, activation='relu')(x)                                    # Classify the data into 100 features, make all negatives 0
    x = Dropout(dropout)(x)                                                      # Randomly drop out (turn off) 10% of the neurons (Prevent overfitting)
    x = Dense(50, activation='relu')(x)                                     # Classify the data into 50 features, make all negatives 0
    x = Dropout(dropout)(x)                                                      # Randomly drop out 10% of the neurons (Prevent overfitting)
    
    #categorical output of the angle
    angle_out = Dense(5, activation='softmax', name='angle_out')(x)        # Connect every input with every output and output 15 hidden units. Use Softmax to give percentage. 15 categories and find best one based off percentage 0.0-1.0
    
    model = Model(input=[img_in], output=[angle_out])
    
    model.compile(optimizer='adam',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    return model



#______________________________________________________________________________________________
#Implementing the ResNet
def identity_block(X, f1 , f2 , f3, filters, stage, block):
    """
    Implementation of the identity block as defined in Figure 3
    
    Arguments:
    X -- input tensor of shape (m, n_H_prev, n_W_prev, n_C_prev)
    f -- integer, specifying the shape of the middle CONV's window for the main path
    filters -- python list of integers, defining the number of filters in the CONV layers of the main path
    stage -- integer, used to name the layers, depending on their position in the network
    block -- string/character, used to name the layers, depending on their position in the network
    
    Returns:
    X -- output of the identity block, tensor of shape (n_H, n_W, n_C)
    """
    
    import numpy as np
    from keras import layers
    from keras.layers import Input, Add, Dense, Activation, ZeroPadding2D, BatchNormalization, Flatten, Conv2D, AveragePooling2D, MaxPooling2D, GlobalMaxPooling2D
    from keras.models import Model, load_model
    from keras.preprocessing import image
    from keras.utils import layer_utils
    from keras.utils.data_utils import get_file
    from keras.applications.imagenet_utils import preprocess_input
    import pydot
    from IPython.display import SVG
    from keras.utils.vis_utils import model_to_dot
    from keras.utils import plot_model
    from keras.initializers import glorot_uniform
    import scipy.misc
    from matplotlib.pyplot import imshow
    
    
    # defining name basis
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'
    
    # Retrieve Filters
    F1, F2, F3 = filters
    
    # Save the input value. You'll need this later to add back to the main path. 
    X_shortcut = X
    
    # First component of main path
    X = Conv2D(filters = F1, kernel_size = (f1, f1), strides = (1,1), padding = 'valid', name = conv_name_base + '2a', kernel_initializer = glorot_uniform(seed=0))(X)
    X = BatchNormalization(axis = -1, name = bn_name_base + '2a')(X)
    X = Activation('relu')(X)

    
    # Second component of main path (≈3 lines)
    X = Conv2D(filters = F2, kernel_size = (f2, f2), strides = (1,1), padding = 'same', name = conv_name_base + '2b', kernel_initializer = glorot_uniform(seed=0))(X)
    X = BatchNormalization(axis = -1, name = bn_name_base + '2b')(X)
    X = Activation('relu')(X)

    # Third component of main path (≈2 lines)
    X = Conv2D(filters = F3, kernel_size = (f3, f3), strides = (1,1), padding = 'valid', name = conv_name_base + '2c', kernel_initializer = glorot_uniform(seed=0))(X)
    X = BatchNormalization(axis = -1, name = bn_name_base + '2c')(X)

    # Final step: Add shortcut value to main path, and pass it through a RELU activation (≈2 lines)
    X = Add()([X, X_shortcut])
    X = Activation('relu')(X)
    
    
    return X

def convolutional_block(X, f1 , f2 , f3, filters, stage, block, s = 2):
    """
    Implementation of the convolutional block as defined in Figure 4
    
    Arguments:
    X -- input tensor of shape (m, n_H_prev, n_W_prev, n_C_prev)
    f -- integer, specifying the shape of the middle CONV's window for the main path
    filters -- python list of integers, defining the number of filters in the CONV layers of the main path
    stage -- integer, used to name the layers, depending on their position in the network
    block -- string/character, used to name the layers, depending on their position in the network
    s -- Integer, specifying the stride to be used
    
    Returns:
    X -- output of the convolutional block, tensor of shape (n_H, n_W, n_C)
    """
    
    import numpy as np
    from keras import layers
    from keras.layers import Input, Add, Dense, Activation, ZeroPadding2D, BatchNormalization, Flatten, Conv2D, AveragePooling2D, MaxPooling2D, GlobalMaxPooling2D
    from keras.models import Model, load_model
    from keras.preprocessing import image
    from keras.utils import layer_utils
    from keras.utils.data_utils import get_file
    from keras.applications.imagenet_utils import preprocess_input
    import pydot
    from IPython.display import SVG
    from keras.utils.vis_utils import model_to_dot
    from keras.utils import plot_model
    from keras.initializers import glorot_uniform
    import scipy.misc
    from matplotlib.pyplot import imshow
    
    
    # defining name basis
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'
    
    # Retrieve Filters
    F1, F2, F3 = filters
    
    # Save the input value
    X_shortcut = X


    ##### MAIN PATH #####
    # First component of main path 
    X = Conv2D(F1, (f1, f1), strides = (s,s), name = conv_name_base + '2a', kernel_initializer = glorot_uniform(seed=0))(X)
    X = BatchNormalization(axis = -1, name = bn_name_base + '2a')(X)
    X = Activation('relu')(X)

    # Second component of main path (≈3 lines)
    X = Conv2D(filters = F2, kernel_size = (f2, f2), strides = (1,1), padding = 'same', name = conv_name_base + '2b', kernel_initializer = glorot_uniform(seed=0))(X)
    X = BatchNormalization(axis = -1, name = bn_name_base + '2b')(X)
    X = Activation('relu')(X)


    # Third component of main path (≈2 lines)
    X = Conv2D(filters = F3, kernel_size = (f3, f3), strides = (1,1), padding = 'valid', name = conv_name_base + '2c', kernel_initializer = glorot_uniform(seed=0))(X)
    X = BatchNormalization(axis = -1, name = bn_name_base + '2c')(X)


    ##### SHORTCUT PATH #### (≈2 lines)
    X_shortcut = Conv2D(filters = F3, kernel_size = (1, 1), strides = (s,s), padding = 'valid', name = conv_name_base + '1',
                        kernel_initializer = glorot_uniform(seed=0))(X_shortcut)
    X_shortcut = BatchNormalization(axis = -1, name = bn_name_base + '1')(X_shortcut)

    # Final step: Add shortcut value to main path, and pass it through a RELU activation (≈2 lines)
    X = Add()([X, X_shortcut])
    X = Activation('relu')(X)
    
    
    return X



def ResNet50(input_shape=(90, 250, 3), classes = 2):
    
    import numpy as np
    from keras import layers
    from keras.layers import Input, Add, Dense, Activation, ZeroPadding2D, BatchNormalization, Flatten, Conv2D, AveragePooling2D, MaxPooling2D, GlobalMaxPooling2D
    from keras.models import Model, load_model
    from keras.preprocessing import image
    from keras.utils import layer_utils
    from keras.utils.data_utils import get_file
    from keras.applications.imagenet_utils import preprocess_input
    import pydot
    from IPython.display import SVG
    from keras.utils.vis_utils import model_to_dot
    from keras.utils import plot_model
    from keras.initializers import glorot_uniform
    import scipy.misc
    from matplotlib.pyplot import imshow
    
    """
    Implementation of the popular ResNet50 the following architecture:
    CONV2D -> BATCHNORM -> RELU -> MAXPOOL -> CONVBLOCK -> IDBLOCK*2 -> CONVBLOCK -> IDBLOCK*3
    -> CONVBLOCK -> IDBLOCK*5 -> CONVBLOCK -> IDBLOCK*2 -> AVGPOOL -> TOPLAYER

    Arguments:
    input_shape -- shape of the images of the dataset
    classes -- integer, number of classes

    Returns:
    model -- a Model() instance in Keras
    """

    # Define the input as a tensor with shape input_shape
    X_input = Input(input_shape)

    # Zero-Padding
    X = ZeroPadding2D((3, 3))(X_input)

    # Stage 1
    X = Conv2D(64, (7, 7), strides=(2, 2), name='conv1', kernel_initializer=glorot_uniform(seed=0))(X)
    X = BatchNormalization(axis=-1, name='bn_conv1')(X)
    X = Activation('relu')(X)
    X = MaxPooling2D((3, 3), strides=(2, 2))(X)

    # Stage 2
    X = convolutional_block(X, f1 = 1, f2 = 3, f3 = 1, filters=[64, 64, 256], stage=2, block='a', s=1)
    X = identity_block(X, f1 = 1, f2 = 3, f3 = 1, filters = [64, 64, 256], stage=2, block='b')
    X = identity_block(X, f1 = 1, f2 = 3, f3 = 1, filters = [64, 64, 256], stage=2, block='c')

    ### START CODE HERE ###

    # Stage 3 (≈4 lines)
    X = convolutional_block(X, f1 = 1, f2 = 3, f3 = 1, filters = [128, 128, 512], stage = 3, block='a', s = 2)
    X = identity_block(X, f1 = 1, f2 = 3, f3 = 1, filters = [128, 128, 512], stage=3, block='b')
    X = identity_block(X, f1 = 1, f2 = 3, f3 = 1, filters = [128, 128, 512], stage=3, block='c')
    X = identity_block(X, f1 = 1, f2 = 3, f3 = 1, filters = [128, 128, 512], stage=3, block='d')

    # Stage 4 (≈6 lines)
    X = convolutional_block(X, f1 = 1, f2 = 3, f3 = 1, filters = [256, 256, 1024], stage = 4, block='a', s = 2)
    X = identity_block(X, f1 = 1, f2 = 3, f3 = 1, filters = [256, 256, 1024], stage=4, block='b')
    X = identity_block(X, f1 = 1, f2 = 3, f3 = 1, filters = [256, 256, 1024], stage=4, block='c')
    X = identity_block(X, f1 = 1, f2 = 3, f3 = 1, filters = [256, 256, 1024], stage=4, block='d')
    X = identity_block(X, f1 = 1, f2 = 3, f3 = 1, filters = [256, 256, 1024], stage=4, block='e')
    X = identity_block(X, f1 = 1, f2 = 3, f3 = 1, filters = [256, 256, 1024], stage=4, block='f')

    # Stage 5 (≈3 lines)
    X = convolutional_block(X, f1 = 1, f2 = 3, f3 = 1, filters = [512, 512, 2048], stage = 5, block='a', s = 2)
    X = identity_block(X, f1 = 1, f2 = 3, f3 = 1, filters = [512, 512, 2048], stage=5, block='b')
    X = identity_block(X, f1 = 1, f2 = 3, f3 = 1,filters = [512, 512, 2048], stage=5, block='c')

    # AVGPOOL (≈1 line). Use "X = AveragePooling2D(...)(X)"
    X = AveragePooling2D((2,2), name="avg_pool")(X)

    ### END CODE HERE ###

    # output layer
    X = Flatten()(X)
    X = Dense(classes, activation='softmax', name='fc' + str(classes), kernel_initializer = glorot_uniform(seed=0))(X)
    
    
    # Create model
    model = Model(inputs = X_input, outputs = X, name='ResNet50')
    model.compile(optimizer='adam',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    return model
    
