# MacTwo
Une mini-voiture autonome pour la course IRONCAR. <br>
![](Mac_Two.png)

# Supervised Learning

On trouve 3 Notebooks dans ce dossier :
<ul> 
    <li><b> classification-all_datasets.ipynb:</b> Notebook de classification avec tous les différents datasets</li>
    <li><b> classification.ipynb:</b> Notebook de classification</li>
    <li><b> Model_review.ipynb:</b> Notebook pour visualiser les Kernels et interpréter le modèle </li> 
    <li><b> Preprocess_camera_really_up.ipynb:</b> Notebook pour le preprocess des données </li>
</ul>

# Models

3 modèles sont dans ce dossier:

<ul> 
    <li><b> classification_angle_final.hdf5</b> C'est le modèle final pour l'angle avant le Transfer Learning</li>
    <li><b> classification_angle_speed_final.hdf5 </b> Modèle final pour l'angle et le freinage </li>
    <li><b> Classification_angle_speed_final_freezing</b> Modèle obtenu en freezant le modèle précédent</li>
</ul>

# fonctionsUtiles

Dossier avec les différentes fonctions utiles

# Datasets

Dossier avec les datasets

    